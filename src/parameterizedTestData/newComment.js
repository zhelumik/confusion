export const commentsToTestPositive = "dishId,rating,author,comment\n" +
  "0,1,verylongnameofa,good comment\n" +
  "10000,1,abc,good comment\n" +
  "0,2,abc,good comment\n" +
  "10000,2,verylongnameofa,good comment\n" +
  "0,3,abc,good comment\n" +
  "10000,3,verylongnameofa,good comment\n" +
  "0,4,abc,good comment\n" +
  "10000,4,verylongnameofa,good comment\n" +
  "0,5,abc,good comment\n" +
  "10000,5,verylongnameofa,good comment";
export const commentsToTestNegative = "dishId,rating,author,comment\n" +
  "0,2,ab,good comment\n" +
  "0,3,verylongnameofau,good comment\n" +
  "10000,4,ab,good comment\n" +
  "10000,5,verylongnameofau,good comment\n" +
  "-1,1,abc,good comment\n" +
  "-1,2,verylongnameofa,good comment\n" +
  "10001,3,abc,good comment\n" +
  "10001,4,verylongnameofa,good comment\n" +
  "10000,1,ab,good comment\n" +
  "0,1,verylongnameofau,good comment\n" +
  "10000,2,verylongnameofau,good comment\n" +
  "10000,3,ab,good comment\n" +
  "0,4,verylongnameofau,good comment\n" +
  "0,5,ab,good comment\n" +
  "10001,1,verylongnameofa,good comment\n" +
  "10001,2,verylongnameofa,good comment\n" +
  "-1,3,abc,good comment\n" +
  "-1,4,abc,good comment\n" +
  "-1,5,abc,good comment\n" +
  "10001,5,verylongnameofa,good comment";