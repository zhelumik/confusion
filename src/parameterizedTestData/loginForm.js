export const loginCredentialsToTestPositive = "username,password,rememberme\n" +
  "abc,gooDp9,true\n" +
  "abc,gooDp9,false\n" +
  "verylongnameofa,gooDp9,true\n" +
  "verylongnameofa,gooDp9,false";
export const loginCredentialsToTestNegative = "username,password,rememberme\n" +
"abc,Short,false\n" +
"abc,nocapital,true\n" +
"verylongnameofa,Short,true\n" +
"verylongnameofa,nocapital,false\n" +
"ab,gooDp9,true\n" +
"verylongnameofau,gooDp9,false\n" +
"verylongnameofau,gooDp9,true\n" +
"ab,gooDp9,false";