export const orderFormToTest = "location,outside,table,customer\n" +
  "central_park,false,near_window,verylongnameofa\n" +
  "main_road,true,near_window,verylongnameofa\n" +
  "main_road,false,near_window,abc\n" +
  "long_street,true,near_window,verylongnameofa\n" +
  "long_street,false,near_window,verylongnameofa\n" +
  "central_park,false,first_floor,abc\n" +
  "main_road,true,first_floor,verylongnameofa\n" +
  "main_road,false,first_floor,abc\n" +
  "long_street,true,first_floor,verylongnameofa\n" +
  "long_street,false,first_floor,verylongnameofa\n" +
  "central_park,false,second_floor,abc\n" +
  "main_road,true,second_floor,abc\n" +
  "main_road,false,second_floor,abc\n" +
  "long_street,true,second_floor,verylongnameofa\n" +
  "long_street,false,second_floor,verylongnameofa\n" +
  "central_park,false,basement,verylongnameofa\n" +
  "main_road,true,basement,verylongnameofa\n" +
  "main_road,false,basement,abc\n" +
  "long_street,true,basement,verylongnameofa\n" +
  "long_street,false,basement,abc";