import React from 'react';
import Home from '../components/HomeComponent';
import BrowserRouter from 'react-router-dom/BrowserRouter';
import { render, cleanup } from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect';
import TestRenderer from 'react-test-renderer';

afterEach(cleanup);

/**
 * @group unit/components
 */
it("renders correctly", () => {
  const promo = {
    id: 0,
    name: 'Weekend Grand Buffet',
    image: '/assets/images/buffet.png',
    label: 'New',
    price: '19.99',
    featured: true,
    description: 'Featuring mouthwatering combinations with a choice of five different salads, six enticing appetizers, six main entrees and five choicest desserts. Free flowing bubbly and soft drinks. All for just $19.99 per person'
  };
  const { getByTestId } = render(
    <BrowserRouter>
      <Home
        dish={{}}
        dishesLoading={false}
        dishesErrMess={''}
        promotion={promo}
        promosLoading={false}
        promosErrMess={''}
        leader={{}}
        leadersLoading={false}
        leadersErrMess={''}
      />
    </BrowserRouter>
  );
  expect(getByTestId("promo").getElementsByClassName("card-title")[0]).toHaveTextContent("Weekend Grand Buffet");
  expect(getByTestId("promo").getElementsByClassName("card-text")[0]).toHaveTextContent("Featuring mouthwatering combinations with a choice of five different salads, six enticing appetizers, six main entrees and five choicest desserts. Free flowing bubbly and soft drinks. All for just $19.99 per person");
});

describe('Snap shot test for promo', () => {
  it('should match with snapshot', () => {
    const promo = {
      id: 0,
      name: 'Weekend Grand Buffet',
      image: '/assets/images/buffet.png',
      label: 'New',
      price: '19.99',
      featured: true,
      description: 'Featuring mouthwatering combinations with a choice of five different salads, six enticing appetizers, six main entrees and five choicest desserts. Free flowing bubbly and soft drinks. All for just $19.99 per person'
    };
    const tree = TestRenderer.create(
      <BrowserRouter>
        <Home
          dish={{}}
          dishesLoading={false}
          dishesErrMess={''}
          promotion={promo}
          promosLoading={false}
          promosErrMess={''}
          leader={{}}
          leadersLoading={false}
          leadersErrMess={''}
        />
      </BrowserRouter>
    )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});