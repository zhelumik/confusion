import React from 'react';
import Dishdetail from '../components/DishdetailComponent';
import BrowserRouter from 'react-router-dom/BrowserRouter';

import '@testing-library/jest-dom/extend-expect';
import TestRenderer from 'react-test-renderer';
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import { baseUrl } from '../shared/baseUrl';

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("renders correctly", () => {
  const dish = {
    id: 1,
    name:'Zucchipakoda',
    image: '/assets/images/zucchipakoda.png',
    category: 'appetizer',
    label:'',
    price:'1.99',
    featured: false,
    description:'Deep fried Zucchini coated with mildly spiced Chickpea flour batter accompanied with a sweet-tangy tamarind sauce'
  };
  const props = {
    dish,
    isLoading: false,
    errMess: '',
    comments: [
    ],
    commentsErrMess: ''
  };
  act(() => {
    render(
      <BrowserRouter>
        <Dishdetail
          dish={props.dish}
          isLoading={props.isLoading}
          errMess={props.errMess}
          comments={props.comments}
          commentsErrMess={props.comments.errMess}
        />
      </BrowserRouter>,
      container
    );
  });
  expect(container.querySelector("[data-testid='dish-name-1']").textContent).toBe("Zucchipakoda");
  expect(container.querySelector("[data-testid='dish-description-1']").textContent).toBe("Deep fried Zucchini coated with mildly spiced Chickpea flour batter accompanied with a sweet-tangy tamarind sauce");
  expect(container.querySelector("[data-testid='dish-img-1']")).toHaveAttribute('src',baseUrl + '/assets/images/zucchipakoda.png');
});

it("renders correctly", () => {
  const dish = {
    id: 3,
    name:'ElaiCheese Cake',
    image: '/assets/images/elaicheesecake.png',
    category: 'dessert',
    label:'',
    price:'2.99',
    featured: false,
    description:'A delectable, semi-sweet New York Style Cheese Cake, with Graham cracker crust and spiced with Indian cardamoms'
  };
  const props = {
    dish,
    isLoading: false,
    errMess: '',
    comments: [
      {
        id: 15,
        dishId: 3,
        rating: 5,
        comment: "Imagine all the eatables, living in conFusion!",
        author: "John Lemon",
        date: "2012-10-16T17:57:28.556094Z"
      },
      {
        id: 16,
        dishId: 3,
        rating: 4,
        comment: "Sends anyone to heaven, I wish I could get my mother-in-law to eat it!",
        author: "Paul McVites",
        date: "2014-09-05T17:57:28.556094Z"
      },
      {
        id: 17,
        dishId: 3,
        rating: 3,
        comment: "Eat it, just eat it!",
        author: "Michael Jaikishan",
        date: "2015-02-13T17:57:28.556094Z"
      },
    ],
    commentsErrMess: ''
  };
  act(() => {
    render(
      <BrowserRouter>
        <Dishdetail
          dish={props.dish}
          isLoading={props.isLoading}
          errMess={props.errMess}
          comments={props.comments}
          commentsErrMess={props.comments.errMess}
        />
      </BrowserRouter>,
      container
    );
  });
  expect(container.querySelector("[data-testid='comment-comment-15']").textContent).toBe("Imagine all the eatables, living in conFusion!");
  expect(container.querySelector("[data-testid='comment-author-15']").textContent).toBe("John Lemon");
  expect(container.querySelector("[data-testid='comment-comment-16']").textContent).toBe("Sends anyone to heaven, I wish I could get my mother-in-law to eat it!");
  expect(container.querySelector("[data-testid='comment-author-16']").textContent).toBe("Paul McVites");
  expect(container.querySelector("[data-testid='comment-comment-17']").textContent).toBe("Eat it, just eat it!");
  expect(container.querySelector("[data-testid='comment-author-17']").textContent).toBe("Michael Jaikishan");

  expect(container.querySelector("[data-testid='dish-name-3']").textContent).toBe("ElaiCheese Cake");
  expect(container.querySelector("[data-testid='dish-description-3']").textContent).toBe("A delectable, semi-sweet New York Style Cheese Cake, with Graham cracker crust and spiced with Indian cardamoms");
  expect(container.querySelector("[data-testid='dish-img-3']")).toHaveAttribute('src',baseUrl + '/assets/images/elaicheesecake.png');
});

it("renders correctly", () => {
  const dish = {
    id: 3,
    name:'ElaiCheese Cake',
    image: '/assets/images/elaicheesecake.png',
    category: 'dessert',
    label:'',
    price:'2.99',
    featured: false,
    description:'A delectable, semi-sweet New York Style Cheese Cake, with Graham cracker crust and spiced with Indian cardamoms'
  };
  const props = {
    dish,
    isLoading: true,
    errMess: '',
    comments: [
      {
        id: 15,
        dishId: 3,
        rating: 5,
        comment: "Imagine all the eatables, living in conFusion!",
        author: "John Lemon",
        date: "2012-10-16T17:57:28.556094Z"
      },
    ],
    commentsErrMess: ''
  };
  act(() => {
    render(
      <BrowserRouter>
        <Dishdetail
          dish={props.dish}
          isLoading={props.isLoading}
          errMess={props.errMess}
          comments={props.comments}
          commentsErrMess={props.comments.errMess}
        />
      </BrowserRouter>,
      container
    );
  });
  expect(container.querySelector("[data-testid='loading']")).toHaveTextContent("Loading . . .");

  expect(container.querySelector("[data-testid='dish-name-3']")).toBe(null);
  expect(container.querySelector("[data-testid='dish-description-3']")).toBe(null);
  expect(container.querySelector("[data-testid='dish-img-3']")).toBe(null);
});


it("renders correctly", () => {
  const dish = {
    id: 3,
    name:'ElaiCheese Cake',
    image: '/assets/images/elaicheesecake.png',
    category: 'dessert',
    label:'',
    price:'2.99',
    featured: false,
    description:'A delectable, semi-sweet New York Style Cheese Cake, with Graham cracker crust and spiced with Indian cardamoms'
  };
  const props = {
    dish,
    isLoading: false,
    errMess: 'Error',
    comments: [
      {
        id: 15,
        dishId: 3,
        rating: 5,
        comment: "Imagine all the eatables, living in conFusion!",
        author: "John Lemon",
        date: "2012-10-16T17:57:28.556094Z"
      },
    ],
    commentsErrMess: ''
  };
  act(() => {
    render(
      <BrowserRouter>
        <Dishdetail
          dish={props.dish}
          isLoading={props.isLoading}
          errMess={props.errMess}
          comments={props.comments}
          commentsErrMess={props.comments.errMess}
        />
      </BrowserRouter>,
      container
    );
  });
  expect(container.querySelector("[data-testid='error-message']")).toHaveTextContent("Error");

  expect(container.querySelector("[data-testid='dish-name-3']")).toBe(null);
  expect(container.querySelector("[data-testid='dish-description-3']")).toBe(null);
  expect(container.querySelector("[data-testid='dish-img-3']")).toBe(null);
});

describe('Snap shot test for DishDetail', () => {
  const dish = {
    id: 1,
    name:'Zucchipakoda',
    image: '/assets/images/zucchipakoda.png',
    category: 'appetizer',
    label:'',
    price:'1.99',
    featured: false,
    description:'Deep fried Zucchini coated with mildly spiced Chickpea flour batter accompanied with a sweet-tangy tamarind sauce'
  };
  const props = {
    dish,
    isLoading: false,
    errMess: '',
    comments: [
    ],
    commentsErrMess: ''
  };
  it('should match with snapshot', () => {
    const tree = TestRenderer.create(
      <BrowserRouter>
        <Dishdetail
          dish={props.dish}
          isLoading={props.isLoading}
          errMess={props.errMess}
          comments={props.comments}
          commentsErrMess={props.comments.errMess}
        />
      </BrowserRouter>
    )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});