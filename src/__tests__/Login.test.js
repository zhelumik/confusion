import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import { login } from '../redux/ActionCreators';
import { loginCredentialsToTestPositive, loginCredentialsToTestNegative } from '../parameterizedTestData/loginForm.js';

let testCases = loginCredentialsToTestPositive.split('\n');
let testCasesJSON = [];
let headers = testCases[0].split(',');
for(let i = 1; i < testCases.length; i++) {
  let data = testCases[i].split(',');
  let obj = {};
  for(let j = 0; j < data.length; j++) {
    obj[headers[j].trim()] = data[j].trim();
  }
  testCasesJSON.push(obj);
}
console.log(JSON.stringify(testCasesJSON));
testCasesJSON.forEach((loginCredentials) => {
  /**
   * @group unit/parameterized
   */
  it("has valid login form", () => {
    expect(login(loginCredentials)).toBe(true);
  });
});

testCases = loginCredentialsToTestNegative.split('\n');
let testCasesNegativeJSON = [];
headers = testCases[0].split(',');
for(let i = 1; i < testCases.length; i++) {
  let data = testCases[i].split(',');
  let obj = {};
  for(let j = 0; j < data.length; j++) {
    obj[headers[j].trim()] = data[j].trim();
  }
  testCasesNegativeJSON.push(obj);
}
console.log(JSON.stringify(testCasesJSON));
testCasesNegativeJSON.forEach((loginCredentials) => {
  /**
   * @group unit/parameterized
   */
  it("has invalid login form", () => {
    expect(login(loginCredentials)).toBe(false);
  });
});