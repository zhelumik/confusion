import React from 'react';
import Header from '../components/HeaderComponent';
import BrowserRouter from 'react-router-dom/BrowserRouter';

import { render, cleanup } from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect';
import TestRenderer from 'react-test-renderer';

afterEach(cleanup);

it("renders correctly", () => {
  const { getByTestId } = render(
    <BrowserRouter>
      <Header />
    </BrowserRouter>
  );
  expect(getByTestId("title")).toHaveTextContent("Ristorante Con Fusion");
  expect(getByTestId("text")).toHaveTextContent("We take inspiration from the World's best cuisines, and create a unique fusion experience. Our lipsmacking creations will tickle your culinary senses!");
});

describe('Snap shot test for Header', () => {
  it('should match with snapshot', () => {
    const tree = TestRenderer.create(
      <BrowserRouter>
        <Header />
      </BrowserRouter>
    )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});