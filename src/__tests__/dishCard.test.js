import React from 'react';
import Home from '../components/HomeComponent';
import BrowserRouter from 'react-router-dom/BrowserRouter';
import { render, cleanup } from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect';
import Dishdetail from '../components/DishdetailComponent';
import { baseUrl } from '../shared/baseUrl';
import TestRenderer from 'react-test-renderer';

afterEach(cleanup);

it("renders correctly", () => {
  const dish = {
    id: 1,
    name:'Zucchipakoda',
    image: '/assets/images/zucchipakoda.png',
    category: 'appetizer',
    label:'',
    price:'1.99',
    featured: false,
    description:'Deep fried Zucchini coated with mildly spiced Chickpea flour batter accompanied with a sweet-tangy tamarind sauce'
  };
  const props = {
    dish,
    isLoading: false,
    errMess: '',
    comments: [
    ],
    commentsErrMess: ''
  };
  const { container } = render(
    <BrowserRouter>
      <Dishdetail
        dish={props.dish}
        isLoading={props.isLoading}
        errMess={props.errMess}
        comments={props.comments}
        commentsErrMess={props.comments.errMess}
      />
    </BrowserRouter>
  );
  expect(container.querySelector("[data-testid='dish-name-1']").textContent).toBe("Zucchipakoda");
  expect(container.querySelector("[data-testid='dish-description-1']").textContent).toBe("Deep fried Zucchini coated with mildly spiced Chickpea flour batter accompanied with a sweet-tangy tamarind sauce");
  expect(container.querySelector("[data-testid='dish-img-1']")).toHaveAttribute('src',baseUrl + '/assets/images/zucchipakoda.png');
});

/**
 * @group unit/components
 */
it("renders correctly", () => {
  const dish = {
    "id": 1,
    "name": "Zucchipakoda",
    "image": "images/zucchipakoda.png",
    "category": "appetizer",
    "label": "",
    "price": "1.99",
    "featured": false,
    "description": "Deep fried Zucchini coated with mildly spiced Chickpea flour batter accompanied with a sweet-tangy tamarind sauce"
  };
  const { getByTestId } = render(
    <BrowserRouter>
      <Home
        dish={dish}
        dishesLoading={false}
        dishesErrMess={''}
        promotion={{}}
        promosLoading={false}
        promosErrMess={''}
        leader={{}}
        leadersLoading={false}
        leadersErrMess={''}
      />
    </BrowserRouter>
  );
  expect(getByTestId("dish").getElementsByClassName("card-title")[0]).toHaveTextContent("Zucchipakoda");
  expect(getByTestId("dish").getElementsByClassName("card-text")[0]).toHaveTextContent("Deep fried Zucchini coated with mildly spiced Chickpea flour batter accompanied with a sweet-tangy tamarind sauce");
});

describe('Snap shot test for dish', () => {
  it('should match with snapshot', () => {
    const dish = {
      "id": 1,
      "name": "Zucchipakoda",
      "image": "images/zucchipakoda.png",
      "category": "appetizer",
      "label": "",
      "price": "1.99",
      "featured": false,
      "description": "Deep fried Zucchini coated with mildly spiced Chickpea flour batter accompanied with a sweet-tangy tamarind sauce"
    };
    const tree = TestRenderer.create(
      <BrowserRouter>
        <Home
          dish={dish}
          dishesLoading={false}
          dishesErrMess={''}
          promotion={{}}
          promosLoading={false}
          promosErrMess={''}
          leader={{}}
          leadersLoading={false}
          leadersErrMess={''}
        />
      </BrowserRouter>
    )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});