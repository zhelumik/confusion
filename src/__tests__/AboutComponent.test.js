import React from 'react';
import About from '../components/AboutComponent';
import BrowserRouter from 'react-router-dom/BrowserRouter';

import { render, cleanup } from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect';
import TestRenderer from 'react-test-renderer';

afterEach(cleanup);

it("renders correctly", () => {
  const leaders = [
    {
      "id": 0,
      "name": "Peter Pan",
      "image": "images/alberto.png",
      "designation": "Chief Epicurious Officer",
      "abbr": "CEO",
      "featured": false,
      "description": "Our CEO, Peter, credits his hardworking East Asian immigrant parents who undertook the arduous journey to the shores of America with the intention of giving their children the best future. His mother's wizardy in the kitchen whipping up the tastiest dishes with whatever is available inexpensively at the supermarket, was his first inspiration to create the fusion cuisines for which The Frying Pan became well known. He brings his zeal for fusion cuisines to this restaurant, pioneering cross-cultural culinary connections."
    },
  ];
  const props = {
    leaders: leaders,
    isLoading: false,
  };
  const { getByTestId } = render(
    <BrowserRouter>
      <About leaders={props}/>
    </BrowserRouter>
  );
  expect(getByTestId("leader")).toHaveTextContent("Corporate Leadership");
  expect(getByTestId("leader-name-0")).toHaveTextContent("Peter Pan");
  expect(getByTestId("leader-designation-0")).toHaveTextContent("Chief Epicurious Officer");
});

it("shows loading text", () => {
  const props = {
    leaders: [],
    isLoading: true,
  };
  const { getByTestId, container } = render(
    <BrowserRouter>
      <About leaders={props}/>
    </BrowserRouter>
  );
  expect(getByTestId("loading")).toHaveTextContent("Loading . . .");

  expect(container.querySelector("[data-testid='leader']")).toBe(null);
  expect(container.querySelector("[data-testid='leader-name-0']")).toBe(null);
  expect(container.querySelector("[data-testid='leader-designation-0']")).toBe(null);
});

it("shows error message", () => {
  const props = {
    leaders: [],
    isLoading: false,
    errMess: "Error"
  };
  const { getByTestId, container } = render(
    <BrowserRouter>
      <About leaders={props}/>
    </BrowserRouter>
  );
  expect(getByTestId("error-message")).toHaveTextContent("Error");

  expect(container.querySelector("[data-testid='leader']")).toBe(null);
  expect(container.querySelector("[data-testid='leader-name-0']")).toBe(null);
  expect(container.querySelector("[data-testid='leader-designation-0']")).toBe(null);
});

describe('Snap shot test for About', () => {
  it('should match with snapshot', () => {
    const leaders = [
      {
        "id": 0,
        "name": "Peter Pan",
        "image": "images/alberto.png",
        "designation": "Chief Epicurious Officer",
        "abbr": "CEO",
        "featured": false,
        "description": "Our CEO, Peter, credits his hardworking East Asian immigrant parents who undertook the arduous journey to the shores of America with the intention of giving their children the best future. His mother's wizardy in the kitchen whipping up the tastiest dishes with whatever is available inexpensively at the supermarket, was his first inspiration to create the fusion cuisines for which The Frying Pan became well known. He brings his zeal for fusion cuisines to this restaurant, pioneering cross-cultural culinary connections."
      },
    ];
    const props = {
      leaders: leaders,
      isLoading: false,
    };
    const tree = TestRenderer.create(
        <BrowserRouter>
          <About leaders={props}/>
        </BrowserRouter>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});