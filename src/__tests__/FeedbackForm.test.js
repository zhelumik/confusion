import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import { validateFeedback } from '../redux/ActionCreators';
import { feedbackToTestPositive, feedbackToTestNegative } from '../parameterizedTestData/feedbackForm.js';

let testCases = feedbackToTestPositive.split('\n');
let testCasesJSON = [];
let headers = testCases[0].split(',');
for(let i = 1; i < testCases.length; i++) {
  let data = testCases[i].split(',');
  let obj = {};
  for(let j = 0; j < data.length; j++) {
    obj[headers[j].trim()] = data[j].trim();
  }
  testCasesJSON.push(obj);
}
console.log(JSON.stringify(testCasesJSON));
testCasesJSON.forEach((feedback) => {
  /**
   * @group unit/parameterized
   */
  it("has valid New Feedback form", () => {
    expect(validateFeedback(feedback)).toBe(true);
  });
});

testCases = feedbackToTestNegative.split('\n');
let testCasesNegativeJSON = [];
headers = testCases[0].split(',');
for(let i = 1; i < testCases.length; i++) {
  let data = testCases[i].split(',');
  let obj = {};
  for(let j = 0; j < data.length; j++) {
    obj[headers[j].trim()] = data[j].trim();
  }
  testCasesNegativeJSON.push(obj);
}
console.log(JSON.stringify(testCasesJSON));
testCasesNegativeJSON.forEach((feedback) => {
  /**
   * @group unit/parameterized
   */
  it("has invalid New Feedback form", () => {
    expect(validateFeedback(feedback)).toBe(false);
  });
});