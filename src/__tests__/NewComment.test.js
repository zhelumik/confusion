import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import { validateComment } from '../redux/ActionCreators';
import { commentsToTestPositive, commentsToTestNegative } from '../parameterizedTestData/newComment.js';

let testCases = commentsToTestPositive.split('\n');
let testCasesPositiveJSON = [];
let headers = testCases[0].split(',');
for(let i = 1; i < testCases.length; i++) {
  let data = testCases[i].split(',');
  let obj = {};
  for(let j = 0; j < data.length; j++) {
    obj[headers[j].trim()] = data[j].trim();
  }
  testCasesPositiveJSON.push(obj);
}
// console.log(JSON.stringify(testCasesJSON));
testCasesPositiveJSON.forEach((comment) => {
  /**
   * @group unit/parameterized
   */
  it("has valid New Comment form", () => {
    expect(validateComment(comment)).toBe(true);
  });
});

testCases = commentsToTestNegative.split('\n');
let testCasesNegativeJSON = [];
headers = testCases[0].split(',');
for(let i = 1; i < testCases.length; i++) {
  let data = testCases[i].split(',');
  let obj = {};
  for(let j = 0; j < data.length; j++) {
    obj[headers[j].trim()] = data[j].trim();
  }
  testCasesPositiveJSON.push(obj);
}
testCasesNegativeJSON.forEach((comment) => {
  /**
   * @group unit/parameterized
   */
  it("has invalid New Comment form", () => {
    expect(validateComment(comment)).toBe(false);
  });
});