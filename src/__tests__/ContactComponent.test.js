import React from 'react';
import Contact from '../components/ContactComponent';
import BrowserRouter from 'react-router-dom/BrowserRouter';

import { render, cleanup } from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect';
import TestRenderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import { ConfigureStore } from '../redux/configureStore';

const store = ConfigureStore();

afterEach(cleanup);

it("renders correctly", () => {
  const { getByTestId } = render(
      <Provider store={store}>
        <BrowserRouter>
          <Contact  />
        </BrowserRouter>
      </Provider>

  );
  expect(getByTestId("feedback-form-title")).toHaveTextContent("Send us your Feedback");
  // expect(getByTestId("text")).toHaveTextContent("We take inspiration from the World's best cuisines, and create a unique fusion experience. Our lipsmacking creations will tickle your culinary senses!");
});

describe('Snap shot test for Contact', () => {
  it('should match with snapshot', () => {
    const tree = TestRenderer.create(
      <Provider store={store}>
        <BrowserRouter>
          <Contact  />
        </BrowserRouter>
      </Provider>
    )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});