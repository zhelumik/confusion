import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import { validateOrder } from '../redux/ActionCreators';
import { orderFormToTest } from '../parameterizedTestData/orderForm.js';

let testCases = orderFormToTest.split('\n');
var testCasesJSON = [];
var headers = testCases[0].split(',');
for(let i = 1; i < testCases.length; i++) {
  let data = testCases[i].split(',');
  let obj = {};
  for(let j = 0; j < data.length; j++) {
    obj[headers[j].trim()] = data[j].trim();
  }
  testCasesJSON.push(obj);
}
// console.log(JSON.stringify(testCasesJSON));
testCasesJSON.forEach((order) => {
  /**
   * @group unit/parameterized
   */
  it("has valid New Order form", () => {
    expect(validateOrder(order)).toBe(true);
  });
});