import React from 'react';
import Footer from '../components/FooterComponent';
import BrowserRouter from 'react-router-dom/BrowserRouter';

import { render, cleanup } from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect';
import TestRenderer from 'react-test-renderer';

afterEach(cleanup);

it("renders correctly", () => {
  const { getByTestId } = render(
    <BrowserRouter>
      <Footer />
    </BrowserRouter>
  );
  expect(getByTestId("address-header")).toHaveTextContent("Our Address");
  expect(getByTestId("phone")).toHaveTextContent(": +852 1234 5678");
  expect(getByTestId("fax")).toHaveTextContent(": +852 8765 4321");

});

describe('Snap shot test for Footer', () => {
  it('should match with snapshot', () => {
    const tree = TestRenderer.create(
      <BrowserRouter>
        <Footer />
      </BrowserRouter>
    )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});