import React from 'react';
import Menu from '../components/MenuComponent';
import BrowserRouter from 'react-router-dom/BrowserRouter';

import { render, cleanup } from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect';
import { baseUrl } from "../shared/baseUrl";

afterEach(cleanup);

it("renders correctly", () => {
  const dishes = [
    {
      id: 0,
      name:'Uthappizza',
      image: '/assets/images/uthappizza.png',
      category: 'mains',
      label:'Hot',
      price:'4.99',
      featured: true,
      description:'A unique combination of Indian Uthappam (pancake) and Italian pizza, topped with Cerignola olives, ripe vine cherry tomatoes, Vidalia onion, Guntur chillies and Buffalo Paneer.'
    },
    {
      id: 1,
      name:'Zucchipakoda',
      image: '/assets/images/zucchipakoda.png',
      category: 'appetizer',
      label:'',
      price:'1.99',
      featured: false,
      description:'Deep fried Zucchini coated with mildly spiced Chickpea flour batter accompanied with a sweet-tangy tamarind sauce'
    },
  ];
  const props = {
    dishes: dishes,
    isLoading: false,
  };
  const { getByTestId } = render(
    <BrowserRouter>
      <Menu dishes={props}/>
    </BrowserRouter>
  );
  expect(getByTestId("dish-name-0")).toHaveTextContent("Uthappizza");
  expect(getByTestId("dish-name-1")).toHaveTextContent("Zucchipakoda");
  expect(getByTestId("dish-img-0")).toHaveAttribute('src',baseUrl + '/assets/images/uthappizza.png');
});

it("shows loading text", () => {
  const props = {
    dishes: [],
    isLoading: true,
  };
  const { getByTestId } = render(
    <BrowserRouter>
      <Menu dishes={props}/>
    </BrowserRouter>
  );
  expect(getByTestId("loading")).toHaveTextContent("Loading . . .");
});

it("shows error message", () => {
  const props = {
    dishes: [],
    isLoading: false,
    errMess: "Error"
  };
  const { getByTestId } = render(
    <BrowserRouter>
      <Menu dishes={props}/>
    </BrowserRouter>
  );
  expect(getByTestId("error-message")).toHaveTextContent("Error");
});