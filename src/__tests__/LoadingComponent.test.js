import React from 'react';
import { Loading } from '../components/LoadingComponent';

import { render, cleanup } from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect';

afterEach(cleanup);

it("renders correctly", () => {
  const { getByTestId } = render(
      <Loading />
  );
  expect(getByTestId("loading")).toHaveTextContent("Loading . . .");
});
