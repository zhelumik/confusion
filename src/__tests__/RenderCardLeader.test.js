import React from 'react';
import { RenderCard } from '../components/HomeComponent';
import BrowserRouter from 'react-router-dom/BrowserRouter';
import { render, cleanup } from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect';
import TestRenderer from 'react-test-renderer';

afterEach(cleanup);

/**
 * @group unit/components
 */
it("renders correctly", () => {
  const leader = {
    id: 2,
    name: 'Agumbe Tang',
    image: '/assets/images/alberto.png',
    designation: 'Chief Taste Officer',
    abbr: 'CTO',
    featured: false,
    description: 'Blessed with the most discerning gustatory sense, Agumbe, our CFO, personally ensures that every dish that we serve meets his exacting tastes. Our chefs dread the tongue lashing that ensues if their dish does not meet his exacting standards. He lives by his motto, You click only if you survive my lick.'
  };
  const { container } = render(
    <BrowserRouter>
      <RenderCard
        item={leader}
        isLoading={false}
        errMess={''}
      />
    </BrowserRouter>
  );
  expect(container.getElementsByClassName("card-title")[0]).toHaveTextContent("Agumbe Tang");
  expect(container.getElementsByClassName("card-text")[0]).toHaveTextContent("Blessed with the most discerning gustatory sense, Agumbe, our CFO, personally ensures that every dish that we serve meets his exacting tastes. Our chefs dread the tongue lashing that ensues if their dish does not meet his exacting standards. He lives by his motto, You click only if you survive my lick.");
});

describe('Snap shot test for leader', () => {
  it('should match with snapshot', () => {
    const leader = {
      id: 2,
      name: 'Agumbe Tang',
      image: '/assets/images/alberto.png',
      designation: 'Chief Taste Officer',
      abbr: 'CTO',
      featured: false,
      description: 'Blessed with the most discerning gustatory sense, Agumbe, our CFO, personally ensures that every dish that we serve meets his exacting tastes. Our chefs dread the tongue lashing that ensues if their dish does not meet his exacting standards. He lives by his motto, You click only if you survive my lick.'
    };
    const tree = TestRenderer.create(
      <BrowserRouter>
        <RenderCard
          item={leader}
          isLoading={false}
          errMess={''}
        />
      </BrowserRouter>
    )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});