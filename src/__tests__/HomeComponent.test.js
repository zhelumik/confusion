import React from 'react';
import Home from '../components/HomeComponent';
import BrowserRouter from 'react-router-dom/BrowserRouter';

import { render, cleanup } from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect';
import TestRenderer from 'react-test-renderer';

afterEach(cleanup);
/**
 * @group unit/components
 */
describe("home component", () => {
  it("renders correctly", () => {
    const dish = {
      "id": 1,
      "name": "Zucchipakoda",
      "image": "images/zucchipakoda.png",
      "category": "appetizer",
      "label": "",
      "price": "1.99",
      "featured": false,
      "description": "Deep fried Zucchini coated with mildly spiced Chickpea flour batter accompanied with a sweet-tangy tamarind sauce"
    };
    const { getByTestId } = render(
      <BrowserRouter>
        <Home
          dish={dish}
          dishesLoading={false}
          dishesErrMess={''}
          promotion={{}}
          promosLoading={false}
          promosErrMess={''}
          leader={{}}
          leadersLoading={false}
          leadersErrMess={''}
        />
      </BrowserRouter>
    );
    expect(getByTestId("dish").getElementsByClassName("card-title")[0]).toHaveTextContent("Zucchipakoda");
    expect(getByTestId("dish").getElementsByClassName("card-text")[0]).toHaveTextContent("Deep fried Zucchini coated with mildly spiced Chickpea flour batter accompanied with a sweet-tangy tamarind sauce");
  });

  it("shows loading text", () => {
    const { getByTestId, container } = render(
      <BrowserRouter>
        <Home
          dish={{}}
          dishesLoading={true}
          dishesErrMess={''}
          promotion={{}}
          promosLoading={false}
          promosErrMess={''}
          leader={{}}
          leadersLoading={false}
          leadersErrMess={''}
        />
      </BrowserRouter>
    );
    expect(getByTestId("loading")).toHaveTextContent("Loading . . .");
    expect(container.querySelector("[data-testid='promo']").querySelector("[data-testid='item-card']")).toBeInTheDOM();
    expect(container.querySelector("[data-testid='dish']").querySelector("[data-testid='item-card']")).toBe(null);
    expect(container.querySelector("[data-testid='leader']").querySelector("[data-testid='item-card']")).toBeInTheDOM();
  });

  it("shows loading text", () => {
    const { getByTestId, container } = render(
      <BrowserRouter>
        <Home
          dish={{}}
          dishesLoading={false}
          dishesErrMess={''}
          promotion={{}}
          promosLoading={true}
          promosErrMess={''}
          leader={{}}
          leadersLoading={false}
          leadersErrMess={''}
        />
      </BrowserRouter>
    );
    expect(getByTestId("loading")).toHaveTextContent("Loading . . .");
    expect(container.querySelector("[data-testid='dish']").querySelector("[data-testid='item-card']")).toBeInTheDOM();
    expect(container.querySelector("[data-testid='promo']").querySelector("[data-testid='item-card']")).toBe(null);
    expect(container.querySelector("[data-testid='leader']").querySelector("[data-testid='item-card']")).toBeInTheDOM();
  });

  it("shows loading text", () => {
    const { getByTestId, container } = render(
      <BrowserRouter>
        <Home
          dish={{}}
          dishesLoading={false}
          dishesErrMess={''}
          promotion={{}}
          promosLoading={false}
          promosErrMess={''}
          leader={{}}
          leadersLoading={true}
          leadersErrMess={''}
        />
      </BrowserRouter>
    );
    expect(getByTestId("loading")).toHaveTextContent("Loading . . .");
    expect(container.querySelector("[data-testid='dish']").querySelector("[data-testid='item-card']")).toBeInTheDOM();
    expect(container.querySelector("[data-testid='leader']").querySelector("[data-testid='item-card']")).toBe(null);
    expect(container.querySelector("[data-testid='promo']").querySelector("[data-testid='item-card']")).toBeInTheDOM();
  });

  it("shows loading text", () => {
    const { container } = render(
      <BrowserRouter>
        <Home
          dish={{}}
          dishesLoading={false}
          dishesErrMess={'Error'}
          promotion={{}}
          promosLoading={false}
          promosErrMess={'Error'}
          leader={{}}
          leadersLoading={false}
          leadersErrMess={'Error'}
        />
      </BrowserRouter>
    );
    expect(container.querySelector("[data-testid='loading']")).toBe(null);
    expect(container.querySelector("[data-testid='dish']").querySelector("[data-testid='item-card']")).toBe(null);
    expect(container.querySelector("[data-testid='leader']").querySelector("[data-testid='item-card']")).toBe(null);
    expect(container.querySelector("[data-testid='promo']").querySelector("[data-testid='item-card']")).toBe(null);
  });

  it("shows error message for dish", () => {
    const { getByTestId, container } = render(
      <BrowserRouter>
        <Home
          dish={{}}
          dishesLoading={false}
          dishesErrMess={'Error'}
          promotion={{}}
          promosLoading={false}
          promosErrMess={''}
          leader={{}}
          leadersLoading={false}
          leadersErrMess={''}
        />
      </BrowserRouter>
    );
    expect(getByTestId("error-message")).toHaveTextContent("Error");
    expect(container.querySelector("[data-testid='loading']")).toBe(null);
  });

  it("shows error message for promotion", () => {
    const { getByTestId } = render(
      <BrowserRouter>
        <Home
          dish={{}}
          dishesLoading={false}
          dishesErrMess={''}
          promotion={{}}
          promosLoading={false}
          promosErrMess={'Error'}
          leader={{}}
          leadersLoading={false}
          leadersErrMess={''}
        />
      </BrowserRouter>
    );
    expect(getByTestId("error-message")).toHaveTextContent("Error");
  });

  it("shows error message for leader", () => {
    const { getByTestId } = render(
      <BrowserRouter>
        <Home
          dish={{}}
          dishesLoading={false}
          dishesErrMess={''}
          promotion={{}}
          promosLoading={false}
          promosErrMess={''}
          leader={{}}
          leadersLoading={false}
          leadersErrMess={'Error'}
        />
      </BrowserRouter>
    );
    expect(getByTestId("error-message")).toHaveTextContent("Error");
  });
});

/**
 * @group snapshot
 */
describe('Snap shot test for Home', () => {
  it('should match with snapshot', () => {
    const dish = {
      "id": 1,
      "name": "Zucchipakoda",
      "image": "images/zucchipakoda.png",
      "category": "appetizer",
      "label": "",
      "price": "1.99",
      "featured": false,
      "description": "Deep fried Zucchini coated with mildly spiced Chickpea flour batter accompanied with a sweet-tangy tamarind sauce"
    };
    const tree = TestRenderer.create(
      <BrowserRouter>
        <Home
          dish={dish}
          dishesLoading={false}
          dishesErrMess={''}
          promotion={{}}
          promosLoading={false}
          promosErrMess={''}
          leader={{}}
          leadersLoading={false}
          leadersErrMess={''}
        />
      </BrowserRouter>
    )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});