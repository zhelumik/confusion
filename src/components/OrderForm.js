import React, {Component} from 'react';
import {Button, Col, Label, Modal, ModalBody, ModalHeader, Row} from 'reactstrap';
import {Control, Errors, LocalForm} from 'react-redux-form';
import { postOrder } from '../redux/ActionCreators';

class OrderForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false
    };
    this.toggleModal = this.toggleModal.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  toggleModal() {
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });
  }

  handleSubmit(values) {
    this.toggleModal();
    postOrder(values);
  }

  render() {
    return (
      <div>
        <Button outline onClick={this.toggleModal}>
          <span className="fa fa-edit fa-lg"></span> BOOK
        </Button>
        <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
          <ModalHeader toggle={this.toggleModal}>Book a table</ModalHeader>
          <ModalBody>
            <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
              <Row className="form-group">
                <Label htmlFor="location" md={12}>Choose location</Label>
                <Col md={12}>
                  <Control.select model=".location" name="location" className="form-control">
                    <option>Central Park</option>
                    <option>Main road restaurant</option>
                    <option>Long street 12</option>
                  </Control.select>
                </Col>
              </Row>
              <Row className="form-group">
                <div className="form-check">
                  <Label check>
                    <Control.checkbox model=".agree" name="agree"
                                      className="form-check-input"
                    /> {' '}
                    <strong>Table Outside</strong>
                  </Label>
                </div>
              </Row>
              <Row className="form-group">
                <Label htmlFor="table" md={12}>Table</Label>
                <Col md={12}>
                  <Control.select model=".area" name="area" className="form-control">
                    <option>near window</option>
                    <option>first floor</option>
                    <option>second floor</option>
                    <option>basement</option>
                  </Control.select>
                </Col>
              </Row>
              <Row className="form-group">
                <Label htmlFor="customer" md={12}>Your Name</Label>
                <Col md={12}>
                  <Control.text model=".customer" id="customer" name="customer"
                                placeholder="Your Name"
                                className="form-control"
                  />
                  <Errors
                    className="text-danger"
                    model=".author"
                    show="touched"
                  />
                </Col>
              </Row>
              <Row className="form-group">
                <Col>
                  <Button type="submit" color="primary">
                    Submit
                  </Button>
                </Col>
              </Row>
            </LocalForm>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}
export default OrderForm;