import React from 'react';
import {Card, CardImg, CardImgOverlay, CardBody, CardTitle, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Loading } from './LoadingComponent';
import { baseUrl } from "../shared/baseUrl";

function RenderDish({ dish, onClick }) {
  if (dish != null) {
    return (
      <div key={dish.id} className="col-12 col-md-5 m-1">
        <Link to={`/menu/${dish.id}`}>
          <Card>
            <CardImg data-testid={`dish-img-${dish.id}`} width="100%" src={baseUrl + dish.image} alt={dish.name} />
            <CardBody>
              <CardImgOverlay>
                <CardTitle data-testid={`dish-name-${dish.id}`}>{dish.name}</CardTitle>
              </CardImgOverlay>
            </CardBody>
          </Card>
        </Link>
      </div>
    );
  } else {
    return (
      <div></div>
    );
  }
}

const Menu = (props) => {
  const menu = props.dishes.dishes.map((dish) => {
    return (
      <RenderDish key={dish.id} dish={dish} />
    );
  });

  if (props.dishes.isLoading) {
    return(
      <div className="container">
        <div className="row">
          <Loading />
        </div>
      </div>
    );
  } else if (props.dishes.errMess) {
    return(
      <div className="container">
        <div className="row">
          <div className="col-12">
            <h4 data-testid="error-message">{props.dishes.errMess}</h4>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <div className="container">
        <div className="row">
          <Breadcrumb>
            <BreadcrumbItem><Link to='/home'>Home</Link></BreadcrumbItem>
            <BreadcrumbItem active>Home</BreadcrumbItem>
          </Breadcrumb>
        </div>
        <div className="row">
          {menu}
        </div>
      </div>
    );
  }
};

export default Menu;