import ActionTypes from "./ActionTypes";
import { baseUrl } from "../shared/baseUrl";

export const addComment = (comment) => ({
  type: ActionTypes.ADD_COMMENT,
  payload: comment,
});

export const addOrder = (order) => ({
  type: ActionTypes.ADD_ORDER,
  payload: order,
});

export const validateComment = (comment) => {
  console.log(comment);
  if (comment.dishId < 0 || comment.dishId > 10000) {
    return false;
  }
  if (comment.author.length < 3 || comment.author.length > 15) {
    return false;
  }

  return true;
};

export const postComment = (dishId, rating, author, comment) => (dispatch) => {
  const newComment = {
    dishId,
    rating,
    author,
    comment,
  };
  if (!validateComment(newComment)) {
    return false;
  }
  newComment.date = new Date().toISOString();

  return fetch(baseUrl + 'comments', {
    method: 'POST',
    body: JSON.stringify(newComment),
    headers: {
      'Content-Type': 'application/json'
    },
    credentials: 'same-origin',
  })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
        var errMessage = new Error(error.message);
        throw errMessage;
      })
    .then(response => response.json())
    .then(response => dispatch(addComment(response)))
    .catch(error =>  {
      console.log('post comments', error.message);
      alert('Your comment could not be posted\nError: '+error.message);
    });
};

export const validateOrder = (order) => {
  return true;
};
export const postOrder = (order) => (dispatch) => {
  if (!validateOrder(order)) {
    return false;
  }
  order.date = new Date().toISOString();

  return fetch(baseUrl + 'orders', {
    method: 'POST',
    body: JSON.stringify(order),
    headers: {
      'Content-Type': 'application/json'
    },
    credentials: 'same-origin',
  })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
        var errMessage = new Error(error.message);
        throw errMessage;
      })
    .then(response => response.json())
    .then(response => dispatch(addOrder(response)))
    .catch(error =>  {
      console.log('post order', error.message);
      alert('Your order could not be posted\nError: '+error.message);
    });
};

export const fetchDishes = () => (dispatch) => {
  dispatch(dishesLoading(true));

  return fetch(baseUrl + 'dishes')
      .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
        error => {
          var errMessage = new Error(error.message);
          throw errMessage;
      })
    .then(response => response.json())
    .then(dishes => dispatch(addDishes(dishes)))
    .catch(error => dispatch(dishesFailed(error.message)));
};

export const dishesLoading = () => ({
  type: ActionTypes.DISHES_LOADING
});

export const dishesFailed = (errmess) => ({
  type: ActionTypes.DISHES_FAILED,
  payload: errmess
});

export const addDishes = (dishes) => ({
  type: ActionTypes.ADD_DISHES,
  payload: dishes
});

export const fetchComments = () => (dispatch) => {
  return fetch(baseUrl + 'comments')
      .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
        var errMessage = new Error(error.message);
        throw errMessage;
      })
    .then(response => response.json())
    .then(comments => dispatch(addComments(comments)))
    .catch(error => dispatch(commentsFailed(error.message)));
};

export const commentsFailed = (errmess) => ({
  type: ActionTypes.COMMENTS_FAILED,
  payload: errmess
});

export const addComments = (comments) => ({
  type: ActionTypes.ADD_COMMENTS,
  payload: comments
});


export const fetchPromos = () => (dispatch) => {
  dispatch(promosLoading(true));

  return fetch(baseUrl + 'promotions')
    .then(response => {
      if (response.ok) {
        return response;
      } else {
        var error = new Error('Error ' + response.status + ': ' + response.statusText);
        error.response = response;
        throw error;
      }
    },
    error => {
      var errMessage = new Error(error.message);
      throw errMessage;
    })
    .then(response => response.json())
    .then(promos => dispatch(addPromos(promos)))
    .catch(error => dispatch(promosFailed(error.message)));
};

export const promosLoading = () => ({
  type: ActionTypes.PROMOS_LOADING
});

export const promosFailed = (errmess) => ({
  type: ActionTypes.PROMOS_FAILED,
  payload: errmess
});

export const addPromos = (promos) => ({
  type: ActionTypes.ADD_PROMOS,
  payload: promos
});

export const fetchLeaders = () => (dispatch) => {
  dispatch(leadersLoading(true));

  return fetch(baseUrl + 'leaders')
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
        var errMessage = new Error(error.message);
        throw errMessage;
      })
    .then(response => response.json())
    .then(leaders => dispatch(addLeaders(leaders)))
    .catch(error => dispatch(leadersFailed(error.message)));
};

export const leadersLoading = () => ({
  type: ActionTypes.LEADERS_LOADING
});

export const leadersFailed = (errmess) => ({
  type: ActionTypes.LEADERS_FAILED,
  payload: errmess
});

export const addLeaders = (leaders) => ({
  type: ActionTypes.ADD_LEADERS,
  payload: leaders
});

export const validateFeedback = (feedback) => {
  console.log(feedback);
  if (feedback.firstname.length < 3 || feedback.firstname.length > 15) {
    return false;
  }
  if (feedback.lastname.length < 3 || feedback.lastname.length > 15) {
    return false;
  }
  if (feedback.telnum.length < 9 || feedback.telnum.length > 12) {
    return false;
  }
  if (!validateEmail(feedback.email)) {
    return false;
  }

  return true;
};
const validateEmail = (email) => {
  let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};
export const postFeedback = (firstname, lastname, telnum, email, agree, contactType, message) => (dispatch) => {
  const newFeedback = {
    firstname,
    lastname,
    telnum,
    email,
    agree,
    contactType,
    message,
  };
  if (!validateFeedback(newFeedback)) {
    return false;
  }
  newFeedback.date = new Date().toISOString();

  return fetch(baseUrl + 'feedback', {
    method: 'POST',
    body: JSON.stringify(newFeedback),
    headers: {
      'Content-Type': 'application/json'
    },
    credentials: 'same-origin',
  })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
        var errMessage = new Error(error.message);
        throw errMessage;
      })
    .then(response => response.json())
    .then(response => alert('Thank you for your feedback!\n' + JSON.stringify(response)))
    .catch(error =>  {
      console.log('sent feedback', error.message);
      alert('Your feedback could not be sent\nError: ' + error.message);
    });
};
export const login = (loginCredentials) => {
  if (!validatePassword(loginCredentials.password)) {
    return false;
  }
  if (loginCredentials.username.length < 3 || loginCredentials.username.length > 15) {
    return false;
  }
  return true;
};
const validatePassword = (password) => {
  let re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}$/;
  return re.test(String(password));
}